let timerId;
let timerIsOn = 0;
let image = 0;
let imageOfGame = [...document.querySelectorAll(".image-to-show")];

//-------------------------Добавляю кнопки------------------------
let imageBox = document.querySelector(".images-wrapper");
let buttonWrapper = document.createElement("div");
buttonWrapper.className = "button-wrapper";
imageBox.after(buttonWrapper);

buttonWrapper.insertAdjacentHTML("beforeend",'<input type="button" class="button" value="Припинити">');
buttonWrapper.insertAdjacentHTML("beforeend",'<input type="button" class="button" value="Відновити показ">');
//-----------------------------------------------------------------

function timerOn() {
  if (!timerIsOn) {
    timerId = setInterval(imageShow, 3000);
    imageShow();
    timerIsOn = 1;
  }
}

function timerOff() {
  clearInterval(timerId);
  timerIsOn = 0;
}

function imageHide() {
  imageOfGame.forEach((elem) => (elem.hidden = true));
}

function imageShow() {
  imageHide();
  if (image == imageOfGame.length) image = 0;
  imageOfGame[image].hidden = false;
  image = image + 1;
}

function setButtonListener() {
  let buttonAction = document.querySelector(".button-wrapper");
  buttonAction.addEventListener("click", function (e) {
    if (e.target.value == "Відновити показ") timerOn();
    if (e.target.value == "Припинити") timerOff();
  });
}

imageHide();
setButtonListener();
timerOn();
